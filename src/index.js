import controller from "./controller";
import appbundle from "./appbundle";
import feature from "./feature";
import license from "./license";
import system from './system'
import user from './user'


export default (config) => {
    var instance = {};

    instance.controller = controller(config);
    instance.appbundle = appbundle(config);
    instance.feature = feature(config);
    instance.license = license(config);
    instance.system = system(config);
    instance.user = user(config);

    return instance;
}
