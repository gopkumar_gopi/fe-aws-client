import { LambdaClient, LambdaParams } from '../lib/lambda';
import { S3Params, S3Client } from '../lib/s3'
import DynamoDbClient from '../lib/dynamoDb';
import { appBundleSortFieldMap as webappEsSortMapping } from '../helpers/es-mapper';
import ES from '../lib/es';
import {
    APPBUNDLE_S3_URL,
    APP_BUNDLE_TABLE_NAME,
    APP_BUNDLE_ATTR_ID
} from '../helpers/constants';
import {
    AppBundleItemDraft,
    BatchWriteItemParam,
    PutRequestItem
} from '../helpers/dyn-mapper';
const fs = require('fs');

export default (config) => {
    const module = {};
    const awsConfig = config[config.platform].aws;

    // TODO: Investigate why awsConfig.environment doesn't have value from environment variables.
    const environment = process.env.ENVIRONMENT || awsConfig.environment;
    const esEndpoint = process.env.ELASTICSEARCH_HOST || config.elasticsearch.host;
    const esAppbundleIndex = process.env.APPBUNDLE_INDEX || awsConfig.elasticsearch.indices.appbundle.index;
    const esAppbundleDocumentType = process.env.APPBUNDLE_TYPE || awsConfig.elasticsearch.indices.appbundle.type;
    const awsRegion = awsConfig.aws_region;

    const lambdaClient = new LambdaClient(awsRegion);
    const esClient = new ES(esEndpoint);
    const s3Client = new S3Client();
    const dynamoDbClient = new DynamoDbClient(awsRegion);

    /**
     * Method to list the app bundles from elastic search with the filters provided.
     * @param {Number} from
     * @param {Number} size
     * @param {String} sortColumn
     * @param {String} sortOrder
     */
    module.list = async (from, size, sortColumn, sortOrder) => {
        const esSortColumn = webappEsSortMapping(sortColumn);

        const esResponse = await esClient.search(
            esAppbundleIndex, from, size, `_type:${esAppbundleDocumentType}`, esSortColumn, sortOrder
        );

        const appBundles = esResponse.hits.hits.map(hit => {
            return {
                'appBundleId': hit._source.Id,
                'name': hit._source.Name,
                'version': hit._source.Version,
                'description': hit._source.Description,
                'icons': hit._source.Urls ? hit._source.Urls.map(icon => `${APPBUNDLE_S3_URL}/${hit._source.Id}/${icon}`) : [],
                'lastUpdated': hit._source.LastUpdated
            }
        });
        const totalCount = esResponse.hits.total;
        return {
            appBundles: appBundles,
            totalCount: totalCount
        }
    };

    /**
     * App Bundle - Insertion of App Bundle info to dynamodb upload bundle file to s3 and verify and return appbundle metadata
     * @param {*} file - App bundle binary file.
     */
    module.create = async (file) => {
        try {
            const appBundleData = new AppBundleItemDraft();
            const putParams = new BatchWriteItemParam(APP_BUNDLE_TABLE_NAME, new Array(new PutRequestItem(appBundleData)));
            await dynamoDbClient.batchWriteItem(putParams);

            const appBundleId = appBundleData.AppBundleId.S;
            const stream = fs.readFileSync(file.path);
            const params = new S3Params(`app-bundle-data-${environment}`, `tmp/${appBundleId}.bundle`, stream, file);
            await s3Client._fnUpload(params, { partSize: 10 * 1024 * 1024, queueSize: 1 });
            const payload = {
                body: JSON.stringify({ bundleId: appBundleId })
            };
            return lambdaClient._fnInvokeLambda(new LambdaParams('appBundleMetadata', environment, payload));
        } catch (error) {
            console.error(error);
            return {
                statusCode: 500,
                error: error
            }
        }
    };

    /**
     * Method to update the the bundle meta data in Dynamo DB, upload binay and icons.
     * @param {*} req
     */
    module.update = async (req) => {
        const appBundleId = req.params.id;
        req.body.bundleId = appBundleId;
        if(req.file) {
            const stream = fs.readFileSync(req.file.path);
            const params = new S3Params(`app-bundle-data-${environment}`, `tmp/${appBundleId}.bundle`, stream, req.file);
            await s3Client._fnUpload(params, { partSize: 10 * 1024 * 1024, queueSize: 1 });
            const payload = {
                body: JSON.stringify({ bundleId: appBundleId })
            };
            return lambdaClient._fnInvokeLambda(new LambdaParams('appBundleMetadata', environment, payload));
        } else {
            req.body.appBundleIconNames = req.body.appBundleIconNames ? req.body.appBundleIconNames.split(',') : [];
            req.body.dependencyIds = req.body.dependencyIds ? req.body.dependencyIds.split(',') : [];
            const payload = {
                body: JSON.stringify(req.body)
            };
            return lambdaClient._fnInvokeLambda(new LambdaParams('appBundleUpdate', environment, payload))
        }
    };

    /**
     * Get app bundle data by id
     * @param {String} id - app bundle id
     */
    module.get = async (id) => {
        const appBundle = await dynamoDbClient.getItemById(APP_BUNDLE_TABLE_NAME, APP_BUNDLE_ATTR_ID, id).then(result => result.Item);
        if (appBundle === undefined) {
            console.error(`AppBundle with id ${id} was not found.`);
            throw {code: 'BundleNotFound', message: `Bundle with id ${id} was not found in the system!`}
        }
        return {
            appBundle: {
                'bundleId': id,
                'name': appBundle.AppBundleName.S,
                'version': appBundle.AppBundleVersion.S,
                'description': appBundle.AppBundleDescription ? appBundle.AppBundleDescription.S : '',
                'dependencies': appBundle.AppBundleDependencies ? appBundle.AppBundleDependencies.SS : [],
                'icons': appBundle.AppBundleIcons ? appBundle.AppBundleIcons.L.map(icon => `${APPBUNDLE_S3_URL}/${id}/${icon.S}`) : [],
                'lastUpdated': appBundle.LastUpdated ? appBundle.LastUpdated.S : ''
            }
        }
    };

    /**
     * Function to return the ES response for a bundle. Used to check for updates (ES version changes).
     * @param id The ID of the Bundle.
     */
    module.getEsResponse = async (id) => {
        return esClient.get(esAppbundleIndex, esAppbundleDocumentType, id);
    };

    /**
     * Function to refresh the AppBundle ES index to make changes reflecting asap.
     * @returns {Promise<*>} Of the refresh request
     */
    module.refreshEsIndex = async () => {
        return esClient.refreshIndex(esAppbundleIndex);
    };

    return module;
}
