import { LambdaClient, LambdaParams } from '../lib/lambda';
import {userSortFieldMap as webappEsSortMapping} from '../helpers/es-mapper';
import {userFieldMap as webappEsMapping} from '../helpers/es-mapper';
import ES from '../lib/es';

export default (config) => {
    let module = {};
    let awsConfig = config[config.platform].aws;

    // TODO: Investigate why awsConfig.environment doesn't have value from environment variables.
    const environment = process.env.ENVIRONMENT || awsConfig.environment;
    const awsRegion = process.env.AWS_REGION || awsConfig.aws_region;
    const esEndpoint = process.env.ELASTICSEARCH_HOST || config.elasticsearch.host;
    const esUserIndex = process.env.USER_INDEX || awsConfig.elasticsearch.indices.user.index;
    const esUserDocumentType = process.env.USER_TYPE || awsConfig.elasticsearch.indices.user.type;

    const lambdaClient = new LambdaClient(awsRegion);
    const esClient = new ES(esEndpoint);

    module.create = async (userData) => {
        /**
         *  User - Invoke the lambda method to create user.
        */
        const payload = {
            body: JSON.stringify(userData.customer)
        };
        return await lambdaClient._fnInvokeLambda(new LambdaParams('userCreation', environment, payload))
    };

    module.listForAccount = async (accountId, from, size, sortColumn, sortOrder) => {
        const esSortColumn = webappEsSortMapping(sortColumn);

        const esResponse = await esClient.search(
            esUserIndex, from, size, `${webappEsMapping('account')}:${accountId}`, esSortColumn, sortOrder
        );

        const users = esResponse.hits.hits.map(hit => {
            return {
                "userId": hit._id,
                "firstName": hit._source.Firstname,
                "lastName": hit._source.Lastname,
                "email": hit._source.Email,
                "phone": hit._source.Phone,
                "title": hit._source.Title,
                "salutation": hit._source.Salutation,
                "status": hit._source.Status,
                "locale": hit._source.Locale,
                "group": hit._source.Group
            }
        });
        const totalCount = esResponse.hits.total;
        return {
            users: users,
            totalCount: totalCount
        }
    };

    module.listForGroup = async (groups, from, size, sortColumn, sortOrder) => {
        const esSortColumn = webappEsSortMapping(sortColumn);

        const esResponse = await esClient.searchTerms(
            esUserIndex, from, size, webappEsMapping('group'), groups, esSortColumn, sortOrder
        );

        const users = esResponse.hits.hits.map(hit => {
            return {
                "userId": hit._id,
                "firstName": hit._source.Firstname,
                "lastName": hit._source.Lastname,
                "email": hit._source.Email,
                "phone": hit._source.Phone,
                "title": hit._source.Title,
                "salutation": hit._source.Salutation,
                "status": hit._source.Status,
                "locale": hit._source.Locale,
                "accountId": hit._source.Account,
                "group": hit._source.Group,
                "lastUpdated": hit._source.LastUpdated
            }
        });
        const totalCount = esResponse.hits.total;
        return {
            users: users,
            totalCount: totalCount
        }
    };

    module.get = async (id) => {
        return esClient.get(esUserIndex, esUserDocumentType, id);
    };

    module.refreshEsIndex = async () => {
        return esClient.refreshIndex(esUserIndex);
    };

    module.update = async (userData) => {
        /**
         *  User - Invoke the lambda method to update user details.
        */
        const payload = {
            body: JSON.stringify(userData)
        };
        return await lambdaClient._fnInvokeLambda(new LambdaParams('userUpdate', environment, payload))
    };

    return module;
}
