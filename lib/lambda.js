import * as AWS from "aws-sdk"

/**
 * Create LambdaParams
 * @param {String} functionName - The name of the lambda function
 * @param {String} environment  - The environemnt string
 * @param {Object} payload      - The payload object sent to the lambda function
 *
 * Refer https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html#invoke-property
 */
export class LambdaParams {
    constructor(functionName, environment, payload){
        this.FunctionName = `webapp_${functionName}_${environment}`;
        this.InvocationType = 'RequestResponse';
        this.Payload = JSON.stringify(payload);
    }
}

export class LambdaClient {
    constructor(awsRegion) {
        this.lambda = new AWS.Lambda({ region : awsRegion});
    }

    /**
     * Method to invoke any lambda function with given params object
     * @param {LambdaParams} params - The parms object required to invoke a lambda function
     *
     * Refer https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Lambda.html#invoke-property
     */
    _fnInvokeLambda = async (params) => {
        const response = await new Promise((resolve, reject) => {
            this.lambda.invoke(params, (err, data) => {
                if (err) {
                    reject(err);
                } else{
                    resolve(JSON.parse(data.Payload));
                }
            })
        });
        return response;
    }
}
