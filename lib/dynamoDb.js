import * as AWS from 'aws-sdk'

export default class {
    constructor(awsRegion) {
        this.dynamodb = new AWS.DynamoDB({ region: awsRegion, apiVersion: '2012-08-10' });
    }

    /**
     * Method to put or delete multiple items in one or more dynamoDb tables
     * @param params - The params object required to invoke a dynamoDb function
     *
     * Refer https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB.html#batchWriteItem-property
     */
    batchWriteItem = async (params) => {
        const response = await new Promise((resolve, reject) => {
            this.dynamodb.batchWriteItem(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data)
                }
            })
        });
        return response;
    };

    // Function to update an Item in DynamoDb

    updateItem = async function (params) {
        return await this.dynamodb.updateItem(params).promise()
    };

    scan = async function (tableName, keyName) {
        const query = {
            ProjectionExpression: keyName,
            TableName: tableName
        };
        return await this.dynamodb.scan(query).promise();
    };

    getItemById = async (tableName, idKey, idValue) => {
        const query = {
            Key: {
                [idKey]: {
                    S: idValue
                }
            },
            TableName: tableName
        };
        return await this.dynamodb.getItem(query).promise();
    };

    query = async function (tableName, keyName, keyValue, indexName, filterExpression) {
        const query = {
            ExpressionAttributeValues: {
                ':partitionKey': {
                    S: keyValue
                }
            },
            KeyConditionExpression: `${keyName} = :partitionKey`,
            TableName: tableName
        };

        if (filterExpression) {
            query.FilterExpression = filterExpression
        }

        if (indexName !== undefined) {
            query.IndexName = indexName;
        }

        //Since it's possible that DynamoDB will force pagination for the resources, where we don't need pagination in frontend
        //(e.g. licenses),
        //handle pagination on the server side
        let items = [];
        do {
            const dynamoDbResponse = await this.dynamodb.query(query).promise();
            query.ExclusiveStartKey = dynamoDbResponse.LastEvaluatedKey;
            items = items.concat(dynamoDbResponse.Items);
        } while (query.ExclusiveStartKey !== undefined)

        return items;
    };


    distinctQuery = async function (tableName, keyName, keyValue, indexName, existentAttributeFilter, distinctAttribute, distinctAttributeType = 'S') {
        const items = await this.query(tableName, keyName, keyValue, indexName, existentAttributeFilter);

        const seenValues = {};
        return items.filter(item => {
            if (item[distinctAttribute] === undefined) {
                return true;
            }

            if (seenValues.hasOwnProperty(item[distinctAttribute][distinctAttributeType])) {
                return false;
            }
            seenValues[item[distinctAttribute][distinctAttributeType]] = true;

            return true;
        });
    };
}
