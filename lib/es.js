//AWS ES wrapper
//This might be a good place to implement IAM authentication for ES, if we want to make ES cluster available
//from public IPs in the future.
import { Client } from 'elasticsearch';

export default (esEndpoint) => {
    const module = {};
    const esClient = new Client({
        host: `https://${esEndpoint}`
    });

    module.search = async function (index, from, size, query, esSortColumn, sortOrder) {
        return await esClient.search({
            index: index,
            q: query,
            from: from,
            size: size,
            sort: `${esSortColumn}:${sortOrder}`
        });
    };

    module.searchTerms = async function (index, from, size, termField, termValues, esSortColumn, sortOrder) {
        return await esClient.search({
            index: index,
            from: from,
            size: size,
            sort: `${esSortColumn}:${sortOrder}`,
            body: {
                query: {
                    terms: {
                        [termField]: termValues
                    }
                }
            }
        });
    };

    module.termsSearch = async function (index, fieldName, fieldValues) {
        const requestBody = {
            query: {
                terms: {
                  [fieldName]: fieldValues
                }
              }
        };         
        return await esClient.search({
            index: index,
            body: requestBody
        });
    };

    module.andSearch = async function (indices, queries) {
        const requestBody = {
            query: {
              bool : {
                should : [
                    queries
                ]
              }
            }
        }; 
        return await esClient.search({
            index: indices,
            body: requestBody
        });
    };

    module.get = async function (index, type, id) {
        return esClient.get({
            index: index,
            type: type,
            id: id
        });
    };

    module.refreshIndex = async function (index) {
        return esClient.indices.refresh({
            index: index
        });
    };

    module.index = async (index, type, id, data) => {
        return esClient.index({
            index: index,
            type: type,
            id: id,
            body: data
        });
    };

    module.delete = async (index, type, id) => {
        return esClient.delete({
            index: index,
            type: type,
            id: id
        });
    };

    module.missing = async (index, field, from, size) => {
        return await esClient.search({
            index: index,
            from: from,
            size: size,
            body: {
                query: {
                    bool: {
                        must_not: {
                            wildcard: {[field]: "*"}
                        }
                    }
                }
              }
        });
    };

    module.missingAndPrefix = async (index, missingField, searchField, fieldPrefixValue, from, size) => {
        return await esClient.search({
            index: index,
            from: from,
            size: size,
            sort: `${searchField}.lower_case_sort:asc`,
            body: {
                query: {
                    bool: {
                        must_not: {
                            exists: {field: missingField}
                        },
                        must: {
                            match_phrase_prefix: {
                                [searchField]: `${fieldPrefixValue}`
                            }
                        }
                    }
                }
            }
        });
    };

    /**
     * Searches over multiple fields.
     * @param index {string} Index to search on.
     * @param from {number} Starting point of search (pagination).
     * @param size {number} Number of results to fetch (pagination).
     * @param sortColumns {string[]} SortColumns as an array of strings, allows multiple sort fields.
     * @param sortOrder {string} SortOrder, will be applied to all fields.
     * @param fields {string[]} Fields that should be searched, as an array of field names. Fields can be boosted with ^.
     * @param phrase {string} Phrase to search for.
     */
    module.multiMatchSearch = async function(index, from, size, sortColumns, sortOrder, fields, phrase) {
        const options = {
            index: index,
            from: from,
            size: size,
            body: {
                query: {
                    multi_match: {
                        query: phrase,
                        fields: fields
                    }
                },
                sort: sortColumns.map(column => {
                    return {
                        [column]: sortOrder
                    }
                })
            }
        };
        if (!phrase) {
            delete options.body.query;
        }
        return await esClient.search(options);
    };

    /**
     * Searches over multiple fields with simplified params (without sorting and pagination).
     * @param index {object} Index to search on. String if one single index, array of strings in case of multiple.
     * @param fields {string[]} Fields that should be searched, as an array of field names. Fields can be boosted with ^.
     * @param phrase {string} Phrase to search for.
     */
    module.simpleMultiMatchSearch = async function(index, fields, phrase) {
        const options = {
            index: index,
            body: {
                query: {
                    multi_match: {
                        query: phrase,
                        fields: fields
                    }
                }
            }
        };
        if (!phrase) {
            delete options.body.query;
        }
        return await esClient.search(options);
    };

    module.documentsCount = async function(indexName) {
        return await esClient.count({
            index: indexName
        });
    };

    /**
     * Updates the document type with id in the index.
     * @param index Index of the document.
     * @param type Type of the document.
     * @param id Id of the document.
     * @param updates Object containing fields and values to update.
     */
    module.update = async (index, type, id, updates) => {
        const options = {
            index: index,
            type: type,
            id: id,
            body: {
                doc: updates
            }
        };
        return await esClient.update(options);
    };

    return module;
}

