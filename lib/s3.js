import { S3 } from "aws-sdk"

/**
 * Create S3Params
 * @param {String} bucket - The name of the bucket in s3
 * @param {String} key    - The object key (directory name inside)
 * @param {*}      stream - buffer, blob, or stream to upload
 * @param {Object} file   - file meta data
 *
 * Refer https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#upload-property
 */
export class S3Params {
    constructor(bucket, key, stream, file){
        this.Bucket = bucket;
        this.Key = key;
        this.Body = stream;
        if(file.originalname.indexOf('.bundle')) {
            this.ContentType = 'binary/octet-stream'
        } else if (file.originalname.indexOf('.svg')) {
            this.ContentType = 'image/svg+xml'
        } else {
            this.ContentType = 'application/octet-stream'
        }
    }
}

export class S3Client {
    constructor(apiVersion = '2006-03-01') {
        this.s3 = new S3({ apiVersion });
    }

    /**
     * Uploads an arbitrarily sized buffer, blob, or stream, using intelligent concurrent handling of parts if the payload is large enough.
     * @param {S3Params} params - The parms object required to invoke the s3 upload function
     *
     * Refer https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#upload-property
     */
    _fnUpload = async (params, options) => {
        const response = await new Promise((resolve, reject) => {
            this.s3.upload(params, options, (err, data) => {
                if (err) {
                    console.error(err)
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
        return response;
    }
}
