import { IotData, Iot } from 'aws-sdk'

export default class {
    constructor(endpoint, region) {
        this.iotdata = new IotData({ endpoint, region, apiVersion: '2015-05-28' });
        this.iot = new Iot({ region, apiVersion: '2015-05-28' })
    }

    /**
     * Method to get device details from Iot
     * @param {String} thingName - Thing name of the device
     *
     * Refer https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Iot.html#describeThing-property
     */
    getThingDetails = async (thingName) => {
        return await new Promise((resolve, reject) => {
            this.iot.describeThing({ thingName }, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data)
                }
            });
        });
    }

    /**
     * Method to get device shadow from Iot core
     * @param {String} thingName - Thing name of the device
     *
     * Refer https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/IotData.html#getThingShadow-property
     */
    getThingShadow = async (thingName) => {
        const response = await new Promise((resolve, reject) => {
            this.iotdata.getThingShadow({ thingName }, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data)
                }
            })
        })
        return response;
    }

    /**
     * Method to update device shadow in Iot core
     * @param params - The params object required to update thing shadow
     *
     * Refer https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/IotData.html#updateThingShadow-property
     */
    updateThingShadow = async (params) => {
        const response = await new Promise((resolve, reject) => {
            this.iotdata.updateThingShadow(params, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data)
                }
            })
        });
        return response;
    }
}
