import { appBundleSortFieldMap as webappEsSortMapping } from "../helpers/es-mapper";
import { LambdaClient, LambdaParams } from "../lib/lambda";
import ES from "../lib/es";

export default (config => {
    const module = {};
    const awsConfig = config[config.platform].aws;

    // TODO: Investigate why awsConfig.environment doesn't have value from environment variables.
    const environment = process.env.ENVIRONMENT || awsConfig.environment;
    const awsRegion = process.env.AWS_REGION || awsConfig.aws_region;
    const esEndpoint = process.env.ELASTICSEARCH_HOST || config.elasticsearch.host;
    const esFeatureIndex = process.env.FEATURE_INDEX || awsConfig.elasticsearch.indices.feature.index;
    const esFeatureDocumentType = process.env.FEATURE_TYPE || awsConfig.elasticsearch.indices.feature.type;

    const lambdaClient = new LambdaClient(awsRegion);
    const esClient = new ES(esEndpoint);

    //TODO: call lambda directly from frontend
    module.create = async req => {
        /**
         *  Feature - Insertion of Feature info to dynamodb and return the signed URl for the feature file
        */
        let featureDetails = {
            name: req.body.featureName,
            version: req.body.featureVersion,
            description: req.body.featureDescription,
            featureBinary: req.body.featureFileName
        };
        const payload = {
            pathParameters: req.params,
            body: JSON.stringify(featureDetails)
        };

        return lambdaClient._fnInvokeLambda(new LambdaParams('featureCreate', environment, payload));
    };

    module.update = async req => {
        const payload = {
            pathParameters: req.params,
            body: JSON.stringify(req.body)
        };

        return lambdaClient._fnInvokeLambda(new LambdaParams('featureUpdate', environment, payload));
    };

    module.list = async (from, size, sortColumn, sortOrder) => {
        const esSortColumn = webappEsSortMapping(sortColumn);

        const esResponse = await esClient.search(esFeatureIndex, from, size, `_type:${esFeatureDocumentType}`, esSortColumn, sortOrder);

        const features = esResponse.hits.hits.map(hit => {
            return {
                "featureId": hit._source.Id,
                "name": hit._source.Name,
                "version": hit._source.Version,
                "description": hit._source.Description,
                "lastUpdated": hit._source.LastUpdated
            };
        });
        const totalCount = esResponse.hits.total;
        return {
            features: features,
            totalCount: totalCount
        };
    };

    module.get = async id => {
        return esClient.get(esFeatureIndex, esFeatureDocumentType, id);
    };

    /**
     * Function to refresh the Feature ES index to make changes reflecting asap.
     * @returns {Promise<*>} Of the refresh request
     */
    module.refreshEsIndex = async () => {
        return esClient.refreshIndex(esFeatureIndex);
    };

    return module;
});