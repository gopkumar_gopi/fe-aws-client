import { LambdaClient, LambdaParams } from '../lib/lambda';
import DynamoDbClient from '../lib/dynamoDb';
import IotClient from '../lib/iot';
import * as jwt from 'jsonwebtoken';
import ES from '../lib/es';
import LIC from './license';
import { controllerFieldMap, controllerSortFieldMap } from '../helpers/es-mapper';
import { LicenseUpdateItemParam } from '../helpers/dyn-mapper';
import { IotShadowDraft, State } from '../helpers/iot-mapper';
import { PANDA_LICENSE_INDEX, LICENSE_TABLE_NAME, LICENSE_ATTR_ID, LICENSE_ATTR_CUSTOMER_ID, LICENSE_ATTR_ASSIGNED_TO, LICENSE_ATTR_STATUS, LICENSE_ATTR_FEATURE_ID, LICENSE_ATTR_APPBUNDLE_IDS, FEATURE_TABLE_NAME, FEATURE_ATTR_ID, APP_BUNDLE_TABLE_NAME, APP_BUNDLE_ATTR_ID, STATUS_ASSIGNED, STATUS_AVAILABLE, STATUS_REMOVAL_PENDING, CUSTOMER_LICENSE_INDEX, LICENSE_ATTR_PRODUCT_ID } from '../helpers/constants';

export default (config => {
    const module = {};
    const awsConfig = config[config.platform].aws;

    // TODO: Investigate why awsConfig.environment doesn't have value from environment variables.
    const environment = process.env.ENVIRONMENT || awsConfig.environment;
    const esEndpoint = process.env.ELASTICSEARCH_HOST || config.elasticsearch.host;
    const esPandaIndex = process.env.PANDA_INDEX || awsConfig.elasticsearch.indices.panda.index;
    const esPandaDocumentType = process.env.PANDA_TYPE || awsConfig.elasticsearch.indices.panda.type;
    const iotEndpoint = process.env.IOT_ENDPOINT || awsConfig.iotEndpoint;
    const awsRegion = awsConfig.aws_region;

    const lambdaClient = new LambdaClient(awsRegion);

    const esClient = new ES(esEndpoint);

    const iotClient = new IotClient(iotEndpoint, awsRegion);

    const dynamoDbClient = new DynamoDbClient(awsRegion);

    const licensesHelper = new LIC(config);

    //Search for all controllers
    module.searchAll = async function (query, sortColumn = 'serialNumber', sortOrder = 'asc') {
        const controllerFields = ['serialNumber', 'reportedFirmware', 'companyName', 'city', 'postalCode'].map(field => {
            return controllerFieldMap(field);
        });

        const data = await esClient.multiMatchSearch(esPandaIndex, 0, 500, [controllerSortFieldMap(sortColumn)], sortOrder, controllerFields, query);

        const controllerData = data.hits.hits.map(controller => {
            const controllerData = {
                serialNumber: controller._source.SerialNumber,
                lastSeen: controller._source.LastSeen
            };

            if (controller._source.reportedState !== undefined) {
                controllerData.systemVersion = controller._source.reportedState.systemVersion;
            }

            if (controller._source.account !== undefined) {
                controllerData.customer = controller._source.account;
                controllerData.customer.id = controller._source.customerId;
            }

            return controllerData;
        });

        const controllersData = {
            totalCount: data.hits.total,
            controllers: controllerData
        };
        return {
            data: controllersData,
            status: 200
        };
    };

    //get all controllers
    module.getAll = async function (sortColumn = 'serialNumber', sortOrder = 'asc', from, size) {
        //Get all controllers, with pagination
        const query = `_type:${esPandaDocumentType}`;
        const esControllers = await esClient.search(esPandaIndex, from, size, query, controllerSortFieldMap(sortColumn), sortOrder);

        const pandas = esControllers.hits.hits.map(controller => {
            const controllerData = {
                serialNumber: controller._source.SerialNumber,
                lastSeen: controller._source.LastSeen
            };

            if (controller._source.reportedState !== undefined) {
                controllerData.systemVersion = controller._source.reportedState.systemVersion;
            }

            if (controller._source.account !== undefined) {
                controllerData.customer = controller._source.account;
                controllerData.customer.id = controller._source.customerId;
            }

            return controllerData;
        });

        const totalCount = await esClient.documentsCount(esPandaIndex);
        const controllersData = {
            totalCount: totalCount.count,
            controllers: pandas
        };
        return {
            data: controllersData,
            status: 200
        };
    };

    /**
     * Get list of controllers belonging to given account ID.
     * @param {String} accountId
     * @param {String} from - Index of the list indicating the start
     * @param {String} size - Number of items to fetch
     * @param {String} sortColumn - Column name to sort
     * @param {String} sortOrder - String one of 'asc' or 'desc'
     */
    module.listFor = async function (accountId, from, size, sortColumn, sortOrder) {
        const esSortColumn = controllerSortFieldMap(sortColumn);

        const query = `_type:${esPandaDocumentType} AND customerId:${accountId}`;
        const esResponse = await esClient.search(esPandaIndex, from, size, query, esSortColumn, sortOrder);

        const controllers = esResponse.hits.hits.map(hit => {
            const controller = {
                'serialNumber': hit._source.SerialNumber,
                'name': hit._source.name,
                'location': hit._source.location,
                'lastSeen': hit._source.LastSeen,
                'hasUpdate': hit._source.hasUpdate ? hit._source.hasUpdate : false
            };

            if (hit._source.reportedState !== undefined) {
                Object.assign(controller, {
                    'reportedFirmware': hit._source.reportedState.systemVersion
                });
            }
            return controller;
        });
        const totalCount = esResponse.hits.total;
        return {
            controllers: controllers,
            totalCount: totalCount
        };
    };

    /**
     * Get desired shadow state of the device from IoT
     * @param {String} thingName - Serial number of Panda
     */
    module.getDesiredShadowState = async thingName => {
        let currentShadow = await iotClient.getThingShadow(thingName);
        currentShadow = JSON.parse(currentShadow.payload);
        return currentShadow.state.desired || {};
    };

    /**
     * Determine if the license need to be assigned to a Panda based on content if already availbe as part of other licenses.
     * @param {String} customerId - Id of the customer to whom the Panda is assigned to
     * @param {String} serialNumber - Serial number of the Panda
     * @param {object} licenseToBeProcessed - license from the DynamoDB Licenses Table
     * @param {Boolean} assign - Boolean indicating whether to assign or remove
     * @returns {String} - One of 'assigned', 'removal pending', 'available' or '' (emptry string)
     */
    module.calculateStatusBasedOnLicenseContent = async (customerId, serialNumber, licenseToBeProcessed, assign) => {
        try {
            // Get all licenses that are attached to this controller, including ones with status 'removal pending'.
            const pandaLicenses = await dynamoDbClient.query(LICENSE_TABLE_NAME, LICENSE_ATTR_ASSIGNED_TO, serialNumber, PANDA_LICENSE_INDEX);
            const licenseId = licenseToBeProcessed[LICENSE_ATTR_ID].S;

            if (assign) {
                if (pandaLicenses.find(license => license.LicenseId.S === licenseId && license.Status.S === STATUS_ASSIGNED)) {
                    console.log(`The license ${licenseId} is already assigned to controller ${serialNumber}`);
                    return '';
                }
                // Assign the license even if the content is already available as part of other licenses.
                return STATUS_ASSIGNED;
            } else {
                if (pandaLicenses.find(license => license.LicenseId.S === licenseId)) {

                    // Get license content (features / app bundles) of other licenses attached to this controller.
                    const otherAttachedLicenses = pandaLicenses.filter(license => license.LicenseId.S !== licenseId);
                    const appBundleIds = [];
                    const featureIds = [];
                    for (const license of otherAttachedLicenses) {
                        if (license[LICENSE_ATTR_FEATURE_ID]) {
                            featureIds.push(license[LICENSE_ATTR_FEATURE_ID].S);
                        }
                        if (license[LICENSE_ATTR_APPBUNDLE_IDS] && license[LICENSE_ATTR_APPBUNDLE_IDS].SS.length > 0) {
                            for (const appBundleId of license[LICENSE_ATTR_APPBUNDLE_IDS].SS) {
                                appBundleIds.push(appBundleId.S);
                            }
                        }
                    }

                    // If the license to be removed has a feature already available with another license - release the license
                    if (licenseToBeProcessed[LICENSE_ATTR_FEATURE_ID] && featureIds.find(featureId => featureId === licenseToBeProcessed[LICENSE_ATTR_FEATURE_ID].S)) {
                        return STATUS_AVAILABLE;
                    }
                    // If the license to be removed has all the app bundles already available with other licenses - release the license
                    if (licenseToBeProcessed[LICENSE_ATTR_APPBUNDLE_IDS]) {
                        let appBundleIdsCount = licenseToBeProcessed[LICENSE_ATTR_APPBUNDLE_IDS].SS.length;
                        for (const appBundleId of licenseToBeProcessed[LICENSE_ATTR_APPBUNDLE_IDS].SS) {
                            if (appBundleIds.find(id => id === appBundleId.S)) {
                                appBundleIdsCount--;
                            }
                        }
                        if (appBundleIdsCount < 1) {
                            return STATUS_AVAILABLE;
                        }
                    }
                } else {
                    console.log(`The license ${licenseId} to be removed is not attached to the controller ${serialNumber}`);
                    return '';
                }
                return STATUS_REMOVAL_PENDING;
            }
        } catch (err) {
            console.log('Error error calculating status: ' + err.message);
        }
    };

    /**
     * Method to re-create and update the device shadow from License Table - Dynamo DB
     * The device shadow is updated based on the data available in License Table - Dynamo DB, considering that as single source of truth.
     * @param {String} serialNumber - Serial number of Panda
     */
    module.updateDeviceShadow = async serialNumber => {
        try {
            // Get all licenses that belong to this panda, including with status 'removal pending'
            const pandaLicenses = await dynamoDbClient.query(LICENSE_TABLE_NAME, LICENSE_ATTR_ASSIGNED_TO, serialNumber, PANDA_LICENSE_INDEX);

            // Filter to get only licenses with status 'assigned'
            let assignedLicenses = pandaLicenses.filter(license => license.Status.S === STATUS_ASSIGNED);

            // Re-create desired shadow state with features and appbundles from assigned licenses in DynamoDB.
            let desiredFeatures = [];
            let desiredAppBundles = [];
            for (const license of assignedLicenses) {
                if (license[LICENSE_ATTR_FEATURE_ID]) {
                    const feature = await dynamoDbClient.getItemById(FEATURE_TABLE_NAME, FEATURE_ATTR_ID, license[LICENSE_ATTR_FEATURE_ID].S).then(result => result.Item);
                    if (!desiredFeatures.find(licFeature => licFeature.name === feature.FeatureName.S)) {
                        desiredFeatures.push({
                            name: feature.FeatureName.S,
                            version: feature.FeatureVersion ? feature.FeatureVersion.S : '1.0'
                        });
                    }
                }
                if (license[LICENSE_ATTR_APPBUNDLE_IDS] && license[LICENSE_ATTR_APPBUNDLE_IDS].SS.length > 0) {
                    for (const appBundleId of license[LICENSE_ATTR_APPBUNDLE_IDS].SS) {
                        const appBundle = await dynamoDbClient.getItemById(APP_BUNDLE_TABLE_NAME, APP_BUNDLE_ATTR_ID, appBundleId).then(result => result.Item);
                        if (!desiredAppBundles.find(licAppBundle => licAppBundle.name === appBundle.AppBundleName.S)) {
                            desiredAppBundles.push({
                                name: appBundle.AppBundleName.S,
                                version: appBundle.AppBundleVersion ? appBundle.AppBundleVersion.S : '1.0'
                            });
                        }
                    }
                }
            }
            let currentDesiredShadowState = await module.getDesiredShadowState(serialNumber);
            let systemVersion = currentDesiredShadowState.systemVersion;
            let revision = currentDesiredShadowState.revision ? parseInt(currentDesiredShadowState.revision, 10) + 1 : 1;
            let desiredState = new State(systemVersion, desiredFeatures, desiredAppBundles, revision);
            let desiredShadowDraft = new IotShadowDraft(desiredState);
            let iotParams = {
                payload: new Buffer(JSON.stringify(desiredShadowDraft)),
                thingName: serialNumber
            };
            const updatedShadow = await iotClient.updateThingShadow(iotParams);
            console.log(`Updated device shadow of Panda ${serialNumber}`);
            return updatedShadow;
        } catch (err) {
            console.log('Error updating device shadow: ' + err.message);
        }
    };

    /**
     * Method to attach/release license to/from device in License table DynamoDB
     * @param {String} licenseId - Id of the license
     * @param {String} serialNumber - Serial number of Panda
     * @param {String} status - 'available' | 'removal pending' | 'assigned'
     */
    module.updateDeviceLicense = async (licenseId, serialNumber, status) => {
        try {
            let updatedLicense = await dynamoDbClient.updateItem(new LicenseUpdateItemParam(licenseId, serialNumber, status));
            // Logging - start
            const updatedLicenseStatus = updatedLicense.Attributes[LICENSE_ATTR_STATUS].S;
            const updatedLicenseId = updatedLicense.Attributes[LICENSE_ATTR_ID].S;
            const updatedLicenseAssignedTo = updatedLicense.Attributes[LICENSE_ATTR_ASSIGNED_TO].S;
            switch (updatedLicenseStatus) {
                case STATUS_AVAILABLE:
                    console.log(`Updated license (${updatedLicenseId}) with status '${updatedLicenseStatus}' and detached from Panda '${updatedLicenseAssignedTo}'`);
                    break;
                case STATUS_ASSIGNED:
                    console.log(`Updated license (${updatedLicenseId}) with status '${updatedLicenseStatus}' and attached to Panda '${updatedLicenseAssignedTo}'`);
                    break;
                default:
                    console.log(`Updated license (${updatedLicenseId}) with status '${updatedLicenseStatus}' that is attached to Panda '${updatedLicenseAssignedTo}'`);
                    break;
            }
            // Logging - end
            return updatedLicense;
        } catch (err) {
            console.log('Error updating licenses: ' + err.message);
        }
    };

    /**
     * Method to invoke lambda function deviceAttributesUpdate
     * Updates attributes of the thing in AWS IOT
     * @param {Object} claims - Decoded id token of the authenticated user
     * @param {String} serialNumber - Serial number of Panda
     * @param {String} name - Name as given
     * @param {String} location - Location as given
     */
    module.updateDeviceAttributes = async (claims, serialNumber, name, location) => {
        try {
            const payload = {
                pathParameters: {
                    serialNumber
                },
                body: JSON.stringify({
                    attributes: {
                        name,
                        location
                    }
                }),
                requestContext: {
                    authorizer: {
                        claims
                    }
                }
            };
            let deviceData = await lambdaClient._fnInvokeLambda(new LambdaParams('deviceAttributesUpdate', environment, payload));
            console.log(`Updated device attributes of Panda (${serialNumber}) with name: '${name}' and location: '${location}'`);
            return deviceData;
        } catch (err) {
            console.log(err.message);
        }
    };

    /**
     * Method to update controller attributes (name, location) in IoT, license detail in DynamoDB and device shadow in IoT
     * @param {String} token - id token of the authenticated user
     * @param {String} serialNumber - Serial number of Panda
     * @param {String} name - Name as given
     * @param {String} location - Location as given
     * @param {Object[]} licenses - List of objects containing the license id and action (assign)
     *        [
     *            {
     *                id: $licenseId,
     *                assign: Boolean (undefind will be considered false)
     *            },
     *            ...
     *        ]
     */
    module.updateController = async (token, serialNumber, name, location, licenses) => {
        const decodedToken = jwt.decode(token);
        let deviceData = await module.updateDeviceAttributes(decodedToken, serialNumber, name, location);

        if (licenses && licenses.length > 0) {
            // Check if the license doesn't belong to the account of the Panda - do nothing!
            const controllerDetails = await iotClient.getThingDetails(serialNumber);
            const customerId = controllerDetails.attributes.customerId;
            for (const license of licenses) {
                const licenseData = await dynamoDbClient.query(LICENSE_TABLE_NAME, LICENSE_ATTR_ID, license.id).then(result => result[0]);
                const licenseToBeProcessed = await module.getLicenseToBeProcessed(licenseData, customerId, serialNumber);
                const status = await module.calculateStatusBasedOnLicenseContent(customerId, serialNumber, licenseToBeProcessed, license.assign);
                if (status) {
                    await module.updateDeviceLicense(licenseToBeProcessed[LICENSE_ATTR_ID].S, serialNumber, status);
                }
            }
            await module.updateDeviceShadow(serialNumber);
        }
        return deviceData;
    };

    /**
     * Method to get the license to be processed.
     * Checks if license was assigned to another robot in the meantime and provides alternatives if that is the case.
     * @throws Error 'NoMoreLicenses' if no alternative could be found.
     * @throws Error 'NotOwningLicense' if customer is not the owner of the license (edge case when testing manually).
     * @param licenseToBeProcessed The license to process as returned from the DynamoDb.
     * @param customerId The customer id of the account.
     * @param serialNumber The serial number of the robot.
     * @returns The license for further processing (original or alternative) as returned from DynamoDb.
     */
    module.getLicenseToBeProcessed = async (licenseToBeProcessed, customerId, serialNumber) => {
        const licenseId = licenseToBeProcessed[LICENSE_ATTR_ID].S;
        if (licenseToBeProcessed[LICENSE_ATTR_CUSTOMER_ID].S !== customerId) {
            console.error(`The license doesn't belong to account ${customerId} of the Panda ${serialNumber}`);
            throw { code: 'NotOwningLicense' };
        }

        // Check if license is assigned to another controller - provide alternative!
        if (licenseToBeProcessed[LICENSE_ATTR_ASSIGNED_TO] && licenseToBeProcessed[LICENSE_ATTR_ASSIGNED_TO].S !== serialNumber) {
            console.log(`Cannot assign license ${licenseId}. It is attached to another controller ${licenseToBeProcessed[LICENSE_ATTR_ASSIGNED_TO].S}. ` + 'Searching for alternative license.');
            const nonExistentAttributeFilter = `attribute_not_exists(${LICENSE_ATTR_ASSIGNED_TO})`;
            const productId = licenseToBeProcessed[LICENSE_ATTR_PRODUCT_ID].S;
            const customersLicenses = await dynamoDbClient.query(LICENSE_TABLE_NAME, LICENSE_ATTR_CUSTOMER_ID, customerId, CUSTOMER_LICENSE_INDEX, nonExistentAttributeFilter);
            const sameLicenses = customersLicenses.filter(license => license.ProductId.S === productId);
            if (sameLicenses.length === 0) {
                console.error(`No more licenses found for product ${productId} and customer ${customerId}`);
                throw { code: 'NoMoreLicenses', productId: productId };
            }
            return await module.getLicenseToBeProcessed(sameLicenses[0], customerId, serialNumber);
        }

        return licenseToBeProcessed;
    };

    /**
     * Method to invoke lambda function to assign device to an account (customer)
     * @param {Object} req - The request object
     */
    module.assignDeviceToAccount = async req => {
        const payload = {
            pathParameters: req.params,
            body: JSON.stringify(req.body)
        };

        return lambdaClient._fnInvokeLambda(new LambdaParams('deviceAssignment', environment, payload));
    };

    /**
     * Method to get the information about controller
     * @param {String} serialNumber - Serial number of the contrller (Panda)
     * @param {String} accountId - Account Id to verify if the controller belongs to the account
     * @param {Boolean} isAdmin - Boolean to determine has admin role.
     */
    module.getController = async (serialNumber, accountId, isAdmin) => {
        let esResponse;
        try {
            esResponse = await esClient.get(esPandaIndex, esPandaDocumentType, serialNumber);
        } catch (notFoundError) {
            console.error(`Panda with serial number ${serialNumber} was not found: ${JSON.stringify(notFoundError)}`);
            return {
                data: `Panda with serial number ${serialNumber} was not found!`,
                status: 404
            };
        }

        const pandaOwner = esResponse._source.customerId;
        const name = esResponse._source.name;
        const location = esResponse._source.location;
        let systemVersion;
        if (esResponse._source.reportedState !== undefined) {
            systemVersion = esResponse._source.reportedState.systemVersion;
        }
        const lastSeen = esResponse._source.LastSeen;

        //Admin is allowed to read the info about all the robots, and customer is allowed only to get the data about his robots
        if (!isAdmin && pandaOwner !== accountId) {
            return {
                data: `${accountId} is not the owner of ${serialNumber}`,
                status: 401
            };
        }

        //If the user is allowed to view the panda information, populate the data for licenses management
        const features = await licensesHelper.getFeatureLicenses(pandaOwner, serialNumber);
        const appBundleProducts = await licensesHelper.getAppProductLicenses(pandaOwner, serialNumber);

        const pandaData = {
            serialNumber: serialNumber,
            name: name,
            location: location,
            systemVersion: systemVersion,
            lastSeen: lastSeen,
            features: features,
            appProducts: appBundleProducts
        };

        return {
            data: pandaData,
            status: 200
        };
    };

    /**
     * Function to return the ES response for a panda. Used to check for updates (ES version changes).
     * @param id The serial of the Panda.
     */
    module.getEsResponse = async serial => {
        return esClient.get(esPandaIndex, esPandaDocumentType, serial);
    };

    /**
     * Function to refresh the Panda ES index to make changes reflecting asap.
     * @returns {Promise<*>} Of the refresh request
     */
    module.refreshEsIndex = async () => {
        return esClient.refreshIndex(esPandaIndex);
    };

    /**
     * Method to invoke lambda function to assign multiple devices to an account
     * @param {Object} req - The request object
     */
    module.assignPandasToAccount = async req => {
        const payload = {
            pathParameters: req.params,
            body: JSON.stringify(req.body)
        };

        return lambdaClient._fnInvokeLambda(new LambdaParams('deviceAccountAssignment', environment, payload));
    };

    module.getUnassigned = async (from, size, serialNumberQuery) => {
        let esResponse;

        if (serialNumberQuery) {
            esResponse = await esClient.missingAndPrefix(esPandaIndex, "customerId", "SerialNumber", serialNumberQuery, from, size);
        } else {
            esResponse = await esClient.missing(esPandaIndex, "customerId", from, size);
        }

        const controllers = esResponse.hits.hits;
        const pandas = controllers.map(controller => {
            return {
                serialNumber: controller._source.SerialNumber
            };
        });

        return {
            data: pandas,
            status: 200
        };
    };

    return module;
});