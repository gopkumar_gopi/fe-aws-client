import { LambdaClient, LambdaParams } from "../lib/lambda";
import dynamoDb from "../lib/dynamoDb";

export default (config => {
    const module = {};
    const awsConfig = config[config.platform].aws;

    // TODO: Investigate why awsConfig.environment doesn't have value from environment variables.
    const environment = process.env.ENVIRONMENT || awsConfig.environment;
    const awsRegion = awsConfig.aws_region;
    const lambdaClient = new LambdaClient(awsRegion);
    const dynamoDbClient = new dynamoDb(awsRegion);

    module.updateSystem = async req => {
        /**
         * System - Insertion of system meta info to dynamodb and return the signed URl for the system file and key
        */
        const payload = {
            body: JSON.stringify(req.body)
        };
        return lambdaClient._fnInvokeLambda(new LambdaParams('systemUpdateCreate', environment, payload));
    };

    module.systemVersion = async req => {
        /**
         * System - Retrive the current version of system SystemUpdateVersion
        */
        const systemDetails = await dynamoDbClient.scan(`SystemUpdateMetadata-${environment}`, 'SystemUpdateVersion');
        if (systemDetails.Items.length) {
            return {
                version: systemDetails.Items[0].SystemUpdateVersion.S,
                statusCode: 200
            };
        } else {
            return {
                version: "-",
                statusCode: 200
            };
        }
    };

    return module;
});