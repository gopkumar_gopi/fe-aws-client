import DynamoDbClient from '../lib/dynamoDb';
import { License, LicenseItemDraft, PutRequestItem, BatchWriteItemParam } from '../helpers/dyn-mapper';
import ES from "../lib/es";
import { filterLicenses } from '../helpers/licenseHelper';
import { CUSTOMER_LICENSE_INDEX, LICENSE_TABLE_NAME, LICENSE_ATTR_CUSTOMER_ID, LICENSE_ATTR_FEATURE_ID, LICENSE_ATTR_APPBUNDLE_IDS, APPBUNDLE_S3_URL } from '../helpers/constants';

export default (config => {
    const module = {};
    const awsConfig = config[config.platform].aws;

    const esEndpoint = process.env.ELASTICSEARCH_HOST || config.elasticsearch.host;
    const esAppBundleIndex = process.env.APPBUNDLE_INDEX || awsConfig.elasticsearch.indices.appbundle.index;
    const awsRegion = awsConfig.aws_region;

    const dynamoDbClient = new DynamoDbClient(awsRegion);
    const esClient = new ES(esEndpoint);

    /**
     * Cerate licenses for each product of an order in Dynamo DB
     * @param {Object} order - Order object
     * @param {Object[]} lineItems - List of Line item in the order
     * @param {Object} productTypesIdMap - A map of product type keys mapped to their ids
     */
    module.forOrder = (order, lineItems, productTypesIdMap) => {
        let requestItems = [];
        lineItems.forEach(lineItem => {
            for (let i = 0; i < lineItem.quantity; i++) {
                let item = new LicenseItemDraft({
                    customerId: order.customerId,
                    orderId: order.id,
                    product: lineItem,
                    productId: lineItem.productId,
                    productType: productTypesIdMap[lineItem.productType.id]
                });
                let requestItem = new PutRequestItem(item);
                requestItems.push(requestItem);
            }
        });
        const params = new BatchWriteItemParam(LICENSE_TABLE_NAME, requestItems);
        return dynamoDbClient.batchWriteItem(params);
    };

    /**
     * This method fetch all Feature Product licenses associated with given customerId form Dynamo DB.
     * The status of the license is obtained by checking against given Panda serial number
     * @param {String} customerId
     * @param {String} pandaSerialNumber
     */
    module.getFeatureLicenses = async (customerId, pandaSerialNumber) => {
        const existentAttributeFilter = `attribute_exists(${LICENSE_ATTR_FEATURE_ID})`;
        let featureLicenses = await dynamoDbClient.query(LICENSE_TABLE_NAME, LICENSE_ATTR_CUSTOMER_ID, customerId, CUSTOMER_LICENSE_INDEX, existentAttributeFilter);
        return filterLicenses(featureLicenses.map(license => new License(license, pandaSerialNumber)));
    };

    /**
     * This method fetch all App Product licenses associated with given customerId form Dynamo DB.
     * The status of the license is obtained by checking against given Panda serial number
     * @param {String} customerId
     * @param {String} pandaSerialNumber
     */
    module.getAppProductLicenses = async (customerId, pandaSerialNumber) => {
        const existentAttributeFilter = `attribute_exists(${LICENSE_ATTR_APPBUNDLE_IDS})`;
        let licenses = await dynamoDbClient.query(LICENSE_TABLE_NAME, LICENSE_ATTR_CUSTOMER_ID, customerId, CUSTOMER_LICENSE_INDEX, existentAttributeFilter);
        const appProductLicenses = [];

        for (let license of licenses) {
            let appProductLicense = new License(license, pandaSerialNumber);

            const esResponse = await esClient.termsSearch(esAppBundleIndex, '_id', appProductLicense.appBundleIds);

            appProductLicense.appBundles = esResponse.hits.hits.map(hit => {
                return {
                    id: hit._source.Id,
                    Name: hit._source.Name,
                    icons: hit._source.Urls ? hit._source.Urls.map(icon => `${APPBUNDLE_S3_URL}/${hit._source.Id}/${icon}`) : []
                };
            });

            appProductLicenses.push(appProductLicense);
        }
        return filterLicenses(appProductLicenses);
    };

    return module;
});