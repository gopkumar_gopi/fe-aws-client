const environment = process.env.ENVIRONMENT;

// S3 Bucket Url for App Bundle Assets
export const APPBUNDLE_S3_URL = `https://s3.eu-central-1.amazonaws.com/app-bundle-data-${environment}`

// DynamoDB table names
export const LICENSE_TABLE_NAME    = `Licenses-${environment}`;
export const FEATURE_TABLE_NAME    = `FeaturesMetadata-${environment}`;
export const APP_BUNDLE_TABLE_NAME = `AppBundleMetadata-${environment}`;

// DynamoDB Global Secondary Indices
export const PANDA_LICENSE_INDEX    = 'PandaLicenseIndex';
export const CUSTOMER_LICENSE_INDEX = 'CustomerLicenseIndex';

// DynamoDB table attribute names
export const LICENSE_ATTR_ID            = 'LicenseId';     // Attribute of table Licenses-{env}
export const LICENSE_ATTR_ASSIGNED_TO   = 'AssignedTo';   // Attribute of table Licenses-{env}
export const LICENSE_ATTR_STATUS        = 'Status';       // Attribute of table Licenses-{env}
export const LICENSE_ATTR_CUSTOMER_ID   = 'CustomerId';   // Attribute of table Licenses-{env}
export const LICENSE_ATTR_APPBUNDLE_IDS = 'AppBundleIds'; // Attribute of table Licenses-{env}
export const LICENSE_ATTR_FEATURE_ID    = 'FeatureId';    // Attribute of table Licenses-{env}
export const LICENSE_ATTR_PRODUCT_ID    = 'ProductId';    // Attribute of table Licenses-{env}
export const FEATURE_ATTR_ID            = 'FeatureId';    // Attribute of table FeaturesMetaData-{env}
export const APP_BUNDLE_ATTR_ID         = 'AppBundleId';   // Attribute of table AppBundleMetadata-{env}

// DynamoDB License Statuses
export const STATUS_ASSIGNED        = 'assigned';
export const STATUS_AVAILABLE       = 'available';
export const STATUS_REMOVAL_PENDING = 'removal pending';
