//JS Set doesn't support custom comparators...
export function uniqueArray(sourceArray, distinctAttribute) {
    const seenValues = {};
    return sourceArray.filter(item => {
        if (item[distinctAttribute] === undefined) {
            return true;
        }

        if (seenValues.hasOwnProperty(item[distinctAttribute])) {
            return false;
        }
        seenValues[item[distinctAttribute]] = true;

        return true;
    });
}