/**
 * Class to create the state object
 */
export class State {
    /**
     * State object
     * @param {String} systemVersion
     * @param {Object[]} features
     * @param {Object[]} appBundles
     * @param {String} revision
     */
    constructor (systemVersion, features, appBundles, revision) {
        this.systemVersion = systemVersion;
        this.features = features;
        this.appBundles = appBundles;
        this.revision = revision;
    }
}

/**
 * Class to create device shadow draft
 */
export class IotShadowDraft {
    /**
     * Helper method to create device shadow state (desired)
     * @param {State} desiredState
     */
    constructor (desiredState) {
        this.state = {};
        this.state.desired = desiredState
    }
}
