export function controllerFieldMap(fieldName){
    switch(fieldName) {
        case 'name':
            return 'name';
        case 'location':
            return 'location';
        case 'city':
            return 'account.city';
        case 'postalCode':
            return 'account.postalCode';
        case 'companyName':
            return 'account.companyName';
        case 'reportedFirmware':
            return 'reportedState.systemVersion';
        default:
            return fieldName.charAt(0).toUpperCase() + fieldName.slice(1);
    }
}

export function controllerSortFieldMap(fieldName){
    switch(fieldName) {
        case 'serialNumber':
            return 'SerialNumber.lower_case_sort';
        case 'name':
            return 'name.lower_case_sort';
        case 'location':
            return 'location.lower_case_sort';
        case 'companyName':
            return 'account.companyName.lower_case_sort';
        case 'city':
            return 'account.city.lower_case_sort';
        case 'postalCode':
            return 'account.postalCode.lower_case_sort';
        case 'lastSeen':
            return 'LastSeen';
        case '_score':
            return fieldName;
    }
}

export function appBundleFieldMap(fieldName) {
    switch(fieldName) {
        case 'appBundleId':
            return 'Id';
        case 'icons':
            return 'Urls';
        default:
            return fieldName.charAt(0).toUpperCase() + fieldName.slice(1);
    }
}

export function appBundleSortFieldMap(fieldName) {
    switch(fieldName) {
        case 'name':
            return 'Name.lower_case_sort';
        case 'lastUpdated':
            return 'LastUpdated';
        case '_score':
            return fieldName;
    }
}

export function userFieldMap(fieldName) {
    switch(fieldName) {
        case 'firstName':
            return 'Firstname';
        case 'lastName':
            return 'Lastname';
        default:
            return fieldName.charAt(0).toUpperCase() + fieldName.slice(1);
    }
}

export function userSortFieldMap(fieldName) {
    switch(fieldName) {
        case 'firstName':
            return 'Firstname.lower_case_sort';
        case 'lastName':
            return 'Lastname.lower_case_sort';
        case 'lastUpdated':
            return 'LastUpdated';
        case '_score':
            return fieldName;
        default:
            return fieldName.charAt(0).toUpperCase() + fieldName.slice(1) + '.lower_case_sort';
    }
}

export function accountFieldMap(fieldName) {
    switch(fieldName) {
        case 'externalId':
            return 'sapId';
        case 'city':
            return 'defaultAddress.city';
        case 'postalCode':
            return 'defaultAddress.postalCode';
        case 'streetName':
            return 'defaultAddress.streetName';
        case 'streetNumber':
            return 'defaultAddress.streetNumber';
        case 'phone':
            return 'defaultAddress.phone';
        case 'country':
            return 'defaultAddress.country';
        case 'addressId':
            return 'defaultAddress.addressId';
        default:
            return fieldName;
    }
}

export function accountSortFieldMap(fieldName) {
    switch(fieldName) {
        case 'firstName':
            return ['firstName.lower_case_sort', 'lastName.lower_case_sort'];
        case 'externalId':
            return ['sapId.lower_case_sort'];
        case 'companyName':
            return ['companyName.lower_case_sort', 'defaultAddress.postalCode.lower_case_sort', 'defaultAddress.city.lower_case_sort'];
        case '_score':
            return [fieldName];
    }
}
