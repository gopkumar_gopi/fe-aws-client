import uuid from 'uuid/v4';
import {
    LICENSE_TABLE_NAME,
    LICENSE_ATTR_ID,
    LICENSE_ATTR_ASSIGNED_TO,
    LICENSE_ATTR_STATUS,
    STATUS_REMOVAL_PENDING,
    STATUS_ASSIGNED,
    STATUS_AVAILABLE
} from './constants';

/**
 * Creates license item as required by the frontend from the license record retrieved from Dynamo DB
 */
export class License {
    /**
     * @param {Object} dynamoDbItem
     * @param {String} serialNumber
     */
    constructor (dynamoDbItem, serialNumber) {

        this.licenseId = dynamoDbItem.LicenseId.S;
        this.customerId = dynamoDbItem.CustomerId.S;

        if (dynamoDbItem.Status) {
            this.status = dynamoDbItem.Status.S;
        }

        if (dynamoDbItem.SerialNumber) {
            this.serialNumber = dynamoDbItem.SerialNumber.S;
        }

        if (dynamoDbItem.AssignedTo) {
            this.assignedTo = dynamoDbItem.AssignedTo.S;
        }

        if (dynamoDbItem.FeatureId) {
            this.featureId = dynamoDbItem.FeatureId.S;
        }

        if (dynamoDbItem.AppBundleIds) {
            this.appBundleIds = dynamoDbItem.AppBundleIds.SS;
        }

        if (dynamoDbItem.ProductId) {
            this.productId = dynamoDbItem.ProductId.S;
        }

        if (serialNumber && this.status && this.assignedTo) {
            this.owned = this.assignedTo === serialNumber;
            this.installed = this.assignedTo === serialNumber && this.status === STATUS_ASSIGNED;
            this.disabled = this.status === STATUS_REMOVAL_PENDING || (this.assignedTo && this.assignedTo !== serialNumber);
        }
    }
};

/**
 * Helper class to create a Dynamo DB License Item Draft
 */
export class LicenseItemDraft {
    /**
     * @param {Object} data
     * @param {String} data.customerId - Customer Id
     * @param {String} data.orderId - Oder Id
     * @param {Object} data.product - Order lineitem
     * @param {String} data.productType - Key of the product type (e.g. app_product, robot_product, feature_product etc.)
     */
    constructor(data){
        this.LicenseId = {
            'S': uuid()
        }
        this.CustomerId = {
            'S': data.customerId
        }
        this.OrderId = {
            'S': data.orderId
        }
        this.Status = {
            'S': STATUS_AVAILABLE
        }
        this.ProductId = {
            'S': data.productId
        }

        // Field based on product type
        switch (data.productType) {
            case 'app_product':
                let appbundles;
                if(data.product.dependencies.length > 0){
                    appbundles = data.product.variant.attributes[0].value.concat(data.product.dependencies)
                } else {
                    appbundles = data.product.variant.attributes[0].value
                }
                this.AppBundleIds = {
                    'SS': appbundles
                },
                this.RootBundleIds = {
                    'SS': data.product.rootBundles
                }
                break;
            case 'robot_product':
                this.SerialNumber = {
                    'S': data.product.data
                }
                break;
            case 'feature_product':
                this.FeatureId = {
                    'S': data.product.variant.attributes[0].value
                }
                break;
        }
    }
}

/**
 * Class to create a PutRequestItem of Dynamo DB
 */
export class PutRequestItem {
    constructor (item) {
        this.PutRequest = {
            Item: item
        }
    }
}

/**
 * Class to create parm object to the batchWriteItem method of Dynamo DB client.
 */
export class BatchWriteItemParam {
    /**
     * @param {String} tableName - Dynamo Db table name
     * @param {Object[]} requestItems - List of Dynamo DB Request Item
     */
    constructor (tableName, requestItems) {
        this.RequestItems = {
            [tableName]: requestItems
        }
    }
}

/**
 * Class to create license param object to the updateItem method of Dynamo DB client.
 */
export class LicenseUpdateItemParam {
    constructor(key, assignedTo, status) {
        this.ExpressionAttributeNames = {
            '#SN': LICENSE_ATTR_ASSIGNED_TO,
            '#S': LICENSE_ATTR_STATUS
        };

        this.ExpressionAttributeValues = {
            ':s': {
                S: status
            }
        };

        this.Key = {
            [LICENSE_ATTR_ID]: {
                S: key
            }
        };

        let updateExpression = '';
        if (status === STATUS_AVAILABLE) {
            updateExpression = 'SET #S=:s REMOVE #SN';
        } else {
            this.ExpressionAttributeValues[':sn'] = {
                S: assignedTo
            };
            updateExpression = 'SET #S=:s, #SN=:sn';
        }

        this.UpdateExpression = updateExpression;
        this.ReturnValues = 'ALL_NEW';
        this.TableName = LICENSE_TABLE_NAME;
    };
}


export class AppBundleItemDraft {
    constructor() {
        this.AppBundleId = {
            S: uuid()
        };
        this.AppBundlePublished = {
            BOOL: false
        };
        this.LastUpdated = {
            S: new Date().toLocaleString()
        }
    }
}
