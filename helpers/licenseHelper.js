export function filterLicenses(licenses) {
    const groupedLics = licenses.reduce((acc, license) => {
         if (!acc[license.productId]) {
             acc[license.productId] = [];
         }
 
         acc[license.productId].push(license);
 
         return acc;     
     }, {});
 
     return Object.keys(groupedLics).reduce((acc, productId) => {
         // this if statement is what Gopu suggested, IMO it does not add tons of value so I dont have strong feelings on it :)
         if (groupedLics[productId].length === 1) {
             acc.push(groupedLics[productId][0]);
             return acc;
         }
         // end if
 
         let licenseToDisplay = groupedLics[productId].find(
             license => license.owned
         );
 
         if (licenseToDisplay === undefined) {
             licenseToDisplay = groupedLics[productId].find(
                 license => !license.owned && !license.installed && !license.disabled
             );
         }    
 
         if (licenseToDisplay === undefined) {
             licenseToDisplay = groupedLics[productId][0];
         }
 
         acc.push(licenseToDisplay);
 
         return acc;
     }, []);
 }